<?php

namespace Drupal\Tests\zoomapi\Functional;

use Drupal\key\Entity\Key;
use Drupal\Tests\BrowserTestBase;
use GuzzleHttp\Exception\RequestException;

/**
 * API request tests.
 *
 * @group zoomapi
 */
class ApiRequestTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['zoomapi', 'apitools', 'key'];

  /**
   * A key entity to use for testing.
   *
   * @var \Drupal\key\KeyInterface
   */
  protected $testKey;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // API Secret.
    $this->createTestKey(
      'test_zoomapi_secret',
      'authentication',
      'config'
    );

    // Save Zoom API config.
    $this->config('apitools.client.zoomapi')
      ->set('account_id', 'taco')
      ->set('client_id', 'burrito')
      ->set('client_secret', 'test_zoomapi_secret')
      ->save();
  }

  /**
   * Try to perform an API call, expecting a failure.
   */
  public function testZoomApiConnection() {
    // Post to the webhook controller.
    $client = \Drupal::service('zoomapi.client');
    try {
      $client->get('users');
    }
    catch (RequestException $exception) {
      // We are expecting failure.
      $message = $exception->getMessage();
      $this->assertStringContainsString('400 Bad Request', $message, '400 Bad Request was returned.');
      $this->assertStringContainsString('Invalid client_id or client_secret', $message, 'Asserting client error exists.');
    }
  }

  /**
   * Make a key for testing operations that require a key.
   */
  protected function createTestKey($id, $type = NULL, $provider = NULL) {
    $keyArgs = [
      'id' => $id,
      'label' => "Zoom API Webhook Test Key ($id)",
    ];
    if ($type != NULL) {
      $keyArgs['key_type'] = $type;
    }
    if ($provider != NULL) {
      $keyArgs['key_provider'] = $provider;
    }
    $this->testKey = Key::create($keyArgs);
    $this->testKey->setKeyValue('taco');
    $this->testKey->save();
    return $this->testKey;
  }

}
