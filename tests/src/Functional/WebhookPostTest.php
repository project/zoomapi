<?php

namespace Drupal\Tests\zoomapi\Functional;

use Drupal\Core\Url;
use Drupal\key\Entity\Key;
use Drupal\Tests\BrowserTestBase;

/**
 * Webhook post tests.
 *
 * @group zoomapi
 */
class WebhookPostTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['zoomapi', 'apitools', 'key'];

  /**
   * A key entity to use for testing.
   *
   * @var \Drupal\key\KeyInterface
   */
  protected $testKey;

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create test keys for Key module.
    $this->createTestKey(
      'zoomapi_webhook_secret_token',
      'authentication',
      'config'
    );
    $this->createTestKey(
      'zoomapi_webhook_authorization_key',
      'authentication',
      'config'
    );

    // Save Zoom API config related to webhooks.
    $this->config('apitools.client.zoomapi')
      ->set('event_secret_token', 'zoomapi_webhook_secret_token')
      ->save();
  }

  /**
   * GET method should return a 405.
   */
  public function testGetMethodBlocked() {
    $url = Url::fromRoute('zoomapi.webhooks')
      ->setAbsolute()
      ->toString();

    $res = $this->getHttpClient()->request('GET', $url, ['http_errors' => FALSE]);
    $this->assertEquals(405, $res->getStatusCode(), 'GET method not allowed for the webhooks endpoint.');
  }

  /**
   * Post without authorization or x-zm-signature headers.
   */
  public function testWebhookPostNoAuth() {
    $url = Url::fromRoute('zoomapi.webhooks')
      ->setAbsolute()
      ->toString();
    $options = [
      'headers' => [],
      'http_errors' => FALSE,
      'json' => ['payload' => 'test'],
    ];

    $res = $this->getHttpClient()->request('POST', $url, $options);
    $this->assertEquals(403, $res->getStatusCode(), 'Authorization failure with no headers set.');
  }

  /**
   * Test Webhook Signature and validation.
   */
  public function testWebhookSignature() {
    $timestamp = 1674780079;
    $body = [
      'payload' => [
        'plainToken' => 'qgg8vlvZRS6UYooatFL8Aw',
      ],
      'event_ts' => $timestamp,
      'event' => 'endpoint.url_validation',
    ];
    $bodyString = json_encode($body, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

    $url = Url::fromRoute('zoomapi.webhooks')
      ->setAbsolute()
      ->toString();
    $message = "v0:$timestamp:$bodyString";
    $hashForVerify = hash_hmac('sha256', $message, 'taco');

    // Mock payload.
    $options = [
      'headers' => [
        'x-zm-request-timestamp' => $timestamp,
        'x-zm-signature' => "v0=$hashForVerify",
      ],
      'http_errors' => FALSE,
      'json' => $body,
    ];

    // Post to the webhook controller.
    $res = $this->getHttpClient()->request('POST', $url, $options);
    $payload = (string) $res->getBody();
    $expected = '{"plainToken":"qgg8vlvZRS6UYooatFL8Aw","encryptedToken":"466c50bbda1498ea3f4662e3ec10c2426e0645dabd01e7f7ea82ccc694f8be34"}';
    $this->assertEquals($expected, $payload, 'The expected validation event return matches.');
    $this->assertEquals(200, $res->getStatusCode(), 'The x-zm-signature matches. Access allowed.');

    // Alter the secret token to no longer match.
    $hashForVerify = hash_hmac('sha256', $message, 'burrito');
    $options['headers']['x-zm-signature'] = "v0=$hashForVerify";
    $res = $this->getHttpClient()->request('POST', $url, $options);
    $this->assertEquals(403, $res->getStatusCode(), 'The x-zm-signature does not match. Access denied.');
  }

  /**
   * Make a key for testing operations that require a key.
   */
  protected function createTestKey($id, $type = NULL, $provider = NULL) {
    $keyArgs = [
      'id' => $id,
      'label' => "Zoom API Webhook Test Key ($id)",
    ];
    if ($type != NULL) {
      $keyArgs['key_type'] = $type;
    }
    if ($provider != NULL) {
      $keyArgs['key_provider'] = $provider;
    }
    $this->testKey = Key::create($keyArgs);
    $this->testKey->setKeyValue('taco');
    $this->testKey->save();
    return $this->testKey;
  }

}
