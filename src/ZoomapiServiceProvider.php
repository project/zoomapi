<?php

namespace Drupal\zoomapi;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the container services.
 */
class ZoomapiServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Check if the apitools module is enabled.
    if (!$container->hasDefinition('module_handler') || !$container->get('module_handler')->moduleExists('apitools')) {
      // Remove the zoomapi.client service definition if apitools is missing.
      $container->removeDefinition('zoomapi.client');
    }
  }

}
