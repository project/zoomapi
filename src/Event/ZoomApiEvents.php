<?php

namespace Drupal\zoomapi\Event;

/**
 * Event constants for the Zoom API.
 *
 * @package Drupal\zoomapi\Event
 */
final class ZoomApiEvents {

  /**
   * Zoom webhook posts.
   *
   * @see \Drupal\zoomapi\Event\ZoomApiEvents
   * @var string
   */
  const WEBHOOK_POST = 'zoomapi.webhook.post';

}
